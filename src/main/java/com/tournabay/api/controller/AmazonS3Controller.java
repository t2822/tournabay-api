package com.tournabay.api.controller;

import com.tournabay.api.exception.ForbiddenException;
import com.tournabay.api.model.Team;
import com.tournabay.api.model.Tournament;
import com.tournabay.api.model.User;
import com.tournabay.api.s3.FileType;
import com.tournabay.api.security.CurrentUser;
import com.tournabay.api.security.UserPrincipal;
import com.tournabay.api.service.AmazonS3Service;
import com.tournabay.api.service.TeamService;
import com.tournabay.api.service.TournamentService;
import com.tournabay.api.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/s3")
public class AmazonS3Controller {
    private final TournamentService tournamentService;
    private final AmazonS3Service amazonS3Service;
    private final TeamService teamService;
    private final UserService userService;

    @PutMapping(value = "/upload/logo/tournament/{tournamentId}", consumes = "multipart/form-data")
    @Secured("ROLE_USER")
    public ResponseEntity<?> uploadLogo(@CurrentUser UserPrincipal currentUser, @PathVariable Long tournamentId, @RequestPart MultipartFile file, @RequestParam FileType fileType) {
        Tournament tournament = tournamentService.getTournamentById(tournamentId);
        File logo = amazonS3Service.getFileFromMultipartFile(file);
        amazonS3Service.uploadFile(tournament, logo, fileType);
        amazonS3Service.deleteTemporaryFile(logo);
        return ResponseEntity.ok().build();
    }

    @PutMapping(value = "/upload/logo/tournament/{tournamentId}/team/{teamId}", consumes = "multipart/form-data")
    @Secured("ROLE_USER")
    public ResponseEntity<?> uploadLogo(@CurrentUser UserPrincipal currentUser, @PathVariable Long tournamentId, @PathVariable Long teamId, @RequestPart MultipartFile file, @RequestParam FileType fileType) {
        Tournament tournament = tournamentService.getTournamentById(tournamentId);
        User user = userService.getUserFromPrincipal(currentUser);
        Team team = teamService.getById(teamId, tournament);
        if (!team.getCaptain().getUser().getId().equals(user.getId()))
            throw new ForbiddenException();
        File logo = amazonS3Service.getFileFromMultipartFile(file);
        amazonS3Service.uploadFile(tournament, team, logo, fileType);
        amazonS3Service.deleteTemporaryFile(logo);
        return ResponseEntity.ok().build();
    }
}
