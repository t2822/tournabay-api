package com.tournabay.api.controller;

import com.tournabay.api.dto.DiscordMemberDto;
import com.tournabay.api.exception.BadRequestException;
import com.tournabay.api.model.DiscordData;
import com.tournabay.api.model.Tournament;
import com.tournabay.api.model.User;
import com.tournabay.api.model.discord.GuildJoin;
import com.tournabay.api.model.discord.TournamentDiscordData;
import com.tournabay.api.payload.DiscordBasicRoles;
import com.tournabay.api.payload.DiscordChannelsDetails;
import com.tournabay.api.payload.DiscordNotificationDetails;
import com.tournabay.api.payload.DiscordVerificationDetails;
import com.tournabay.api.repository.GuildJoinRepository;
import com.tournabay.api.security.CurrentUser;
import com.tournabay.api.security.UserPrincipal;
import com.tournabay.api.service.DiscordService;
import com.tournabay.api.service.TournamentService;
import com.tournabay.api.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/discord")
public class DiscordController {
    private final TournamentService tournamentService;
    private final UserService userService;
    private final GuildJoinRepository guildJoinRepository;
    private final DiscordService discordService;

    @PostMapping("/invite-bot/tournament/{tournamentId}")
    @Secured("ROLE_USER")
    public ResponseEntity<String> inviteBot(@CurrentUser UserPrincipal userPrincipal, @PathVariable Long tournamentId, @RequestParam String guildId) {
        User user = userService.getUserFromPrincipal(userPrincipal);
        Tournament tournament = tournamentService.getTournamentById(tournamentId);
        if (tournament.getDiscordData() != null)
            throw new BadRequestException("Tournament already has a discord server!");
        if (user.getDiscordData().isEmpty())
            throw new BadRequestException("You need to connect your discord account first");
        if (tournament.getOwner().getId().equals(user.getId())) {
            DiscordData discordData = user.getDiscordData().stream()
                    .filter(DiscordData::getDefaultDiscord)
                    .findFirst()
                    .orElseThrow(() -> new BadRequestException("Default discord data not found"));
            GuildJoin guildJoin = GuildJoin.builder()
                    .guildId(guildId)
                    .ownerId(discordData.getDiscordId())
                    .tournamentId(tournamentId)
                    .build();
            guildJoinRepository.save(guildJoin);
            return ResponseEntity.ok("https://discord.com/api/oauth2/authorize?client_id=1048375456153489488&permissions=380591532118&scope=bot");
        } else {
            throw new BadRequestException("You are not the owner of this tournament");
        }
    }

    @GetMapping("/members/tournament/{tournamentId}")
    @Secured("ROLE_USER")
    public ResponseEntity<List<DiscordMemberDto>> getServerMembers(@CurrentUser UserPrincipal userPrincipal, @PathVariable Long tournamentId) {
        Tournament tournament = tournamentService.getTournamentById(tournamentId);
        List<DiscordMemberDto> members = discordService.getServerMembers(tournament);
        return ResponseEntity.ok(members);
    }

    @PatchMapping("/verification/tournament/{tournamentId}")
    @Secured("ROLE_USER")
    public ResponseEntity<TournamentDiscordData> updateVerificationDetails(@CurrentUser UserPrincipal userPrincipal, @PathVariable Long tournamentId, @RequestBody DiscordVerificationDetails body) {
        Tournament tournament = tournamentService.getTournamentById(tournamentId);
        TournamentDiscordData tournamentDiscordData = discordService.updateVerificationDetails(tournament, body);
        return ResponseEntity.ok(tournamentDiscordData);
    }

    @PatchMapping("/basic-roles/tournament/{tournamentId}")
    @Secured("ROLE_USER")
    public ResponseEntity<TournamentDiscordData> updateBasicRoles(@CurrentUser UserPrincipal userPrincipal, @PathVariable Long tournamentId, @RequestBody DiscordBasicRoles body) {
        Tournament tournament = tournamentService.getTournamentById(tournamentId);
        TournamentDiscordData tournamentDiscordData = discordService.updateBasicRoles(tournament, body);
        return ResponseEntity.ok(tournamentDiscordData);
    }

    @PatchMapping("/channels/tournament/{tournamentId}")
    @Secured("ROLE_USER")
    public ResponseEntity<TournamentDiscordData> updateChannels(@CurrentUser UserPrincipal userPrincipal, @PathVariable Long tournamentId, @RequestBody DiscordChannelsDetails body) {
        Tournament tournament = tournamentService.getTournamentById(tournamentId);
        TournamentDiscordData tournamentDiscordData = discordService.updateChannels(tournament, body);
        return ResponseEntity.ok(tournamentDiscordData);
    }

    @PatchMapping("/notifications/tournament/{tournamentId}")
    @Secured("ROLE_USER")
    public ResponseEntity<TournamentDiscordData> updateNotificationDetails(@CurrentUser UserPrincipal userPrincipal, @PathVariable Long tournamentId, @RequestBody DiscordNotificationDetails body) {
        Tournament tournament = tournamentService.getTournamentById(tournamentId);
        TournamentDiscordData channelsDetails = discordService.updateNotificationDetails(tournament, body);
        return ResponseEntity.ok(channelsDetails);
    }
}
