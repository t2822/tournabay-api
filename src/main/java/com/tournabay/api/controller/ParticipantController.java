package com.tournabay.api.controller;

import com.tournabay.api.model.Participant;
import com.tournabay.api.model.Tournament;
import com.tournabay.api.model.User;
import com.tournabay.api.payload.UpdateParticipantRequest;
import com.tournabay.api.security.CurrentUser;
import com.tournabay.api.security.UserPrincipal;
import com.tournabay.api.service.ParticipantService;
import com.tournabay.api.service.TournamentService;
import com.tournabay.api.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/participant")
public class ParticipantController {
    private final TournamentService tournamentService;
    private final ParticipantService participantService;
    private final UserService userService;

    /**
     * Add a participant to a tournament if the user has the permission to do so.
     * <p>
     * The `@Secured` annotation is used to ensure that the user is logged in
     *
     * @param osuId        The osu! user id of the user you want to add to the tournament.
     * @param tournamentId The id of the tournament you want to add the participant to.
     * @return A ResponseEntity with the added participant.
     */
    @PostMapping("/add/{osuId}/{tournamentId}")
    @Secured("ROLE_USER")
    @PreAuthorize("hasPermission(#tournamentId, 'Participants')")
    public ResponseEntity<Participant> addParticipant(@CurrentUser UserPrincipal userPrincipal, @PathVariable Long osuId, @PathVariable Long tournamentId) {
        User user = userService.getUserFromPrincipal(userPrincipal);
        Tournament tournament = tournamentService.getTournamentById(tournamentId);
        Participant participant = participantService.getByOsuId(osuId, user, tournament);
        Participant addedParticipant = participantService.addParticipant(tournament, participant);
        return ResponseEntity.ok(addedParticipant);
    }

    /**
     * If the user has the role of ROLE_USER, and if the user has the permission to manage participants, then delete the
     * participant.
     *
     * @param participantId The id of the participant to delete
     * @param tournamentId  The id of the tournament that the participant is in.
     * @return A ResponseEntity with a status code of 200.
     */
    @PostMapping("/delete/{participantId}/{tournamentId}")
    @Secured("ROLE_USER")
    @PreAuthorize("hasPermission(#tournamentId, 'Participants')")
    public ResponseEntity<Void> deleteParticipant(@PathVariable Long participantId, @PathVariable Long tournamentId) {
        Tournament tournament = tournamentService.getTournamentById(tournamentId);
        participantService.deleteById(participantId, tournament);
        return ResponseEntity.ok().build();
    }

    // TODO: Update participant endpoint
    @Secured("ROLE_USER")
    @PatchMapping("/update/{participantId}/{tournamentId}")
    public ResponseEntity<Participant> updateParticipant(@CurrentUser UserPrincipal userPrincipal, @PathVariable Long participantId, @PathVariable Long tournamentId, @RequestBody UpdateParticipantRequest body) {
        Tournament tournament = tournamentService.getTournamentById(tournamentId);
        User user = userService.getUserFromPrincipal(userPrincipal);
        Participant participant = participantService.getById(participantId, tournament);
        Participant updatedParticipant = participantService.updateParticipant(participant, body, tournament);
        return ResponseEntity.ok(updatedParticipant);
    }
}
