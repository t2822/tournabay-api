package com.tournabay.api.controller;

import com.tournabay.api.model.Tournament;
import com.tournabay.api.payload.ChangeRulesRequest;
import com.tournabay.api.security.CurrentUser;
import com.tournabay.api.security.UserPrincipal;
import com.tournabay.api.service.TournamentService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/rules")
public class RulesController {
    private final TournamentService tournamentService;

    @PutMapping("/change/{tournamentId}")
    public ResponseEntity<?> changeRules(@CurrentUser UserPrincipal userPrincipal, @PathVariable Long tournamentId, @RequestBody ChangeRulesRequest body) {
        Tournament tournament = tournamentService.getTournamentById(tournamentId);
        tournament.setRules(body.getRules());
        tournamentService.save(tournament);
        return ResponseEntity.ok("Rules changed");
    }
}
