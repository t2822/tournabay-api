package com.tournabay.api.controller;

import com.tournabay.api.model.Tournament;
import com.tournabay.api.model.User;
import com.tournabay.api.payload.CreateTournamentRequest;
import com.tournabay.api.payload.RegistrationData;
import com.tournabay.api.security.CurrentUser;
import com.tournabay.api.security.UserPrincipal;
import com.tournabay.api.service.TournamentService;
import com.tournabay.api.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api")
public class TournamentController {
    private final UserService userService;
    private final TournamentService tournamentService;

    @GetMapping("/tournament/{id}")
    public ResponseEntity<Tournament> getTournamentById(@PathVariable Long id) {
        Tournament tournament = tournamentService.getTournamentById(id);
        return ResponseEntity.ok(tournament);
    }

    @GetMapping("/tournament/{tournamentId}/bbcode")
    public ResponseEntity<String> getTournamentBBCode(@PathVariable Long tournamentId) {
        Tournament tournament = tournamentService.getTournamentById(tournamentId);
        User user = userService.getByOsuId(3660794L);
        String bbCode = tournamentService.getBBCode(tournament, user);
        return ResponseEntity.ok(bbCode);
    }

    @GetMapping("/tournaments")
    public ResponseEntity<?> getTournaments() {
        return ResponseEntity.ok(tournamentService.getAllTournaments());
    }

    @GetMapping("/my-tournaments")
    public ResponseEntity<List<Tournament>> getMyTournaments(@CurrentUser UserPrincipal userPrincipal) {
        User user = userService.getUserFromPrincipal(userPrincipal);
        List<Tournament> tournaments = tournamentService.getTournamentsByUser(user);
        return ResponseEntity.ok(tournaments);
    }

    @PostMapping("/tournament/create")
    @Secured("ROLE_USER")
    public ResponseEntity<Tournament> createTournament(@CurrentUser UserPrincipal userPrincipal, @RequestBody CreateTournamentRequest body) {
        User user = userService.getUserFromPrincipal(userPrincipal);
        Tournament tournament = tournamentService.createTournament(body, user);
        return ResponseEntity.ok(tournament);
    }

    @PutMapping("/tournament/{tournamentId}/register")
    @Secured("ROLE_USER")
    public ResponseEntity<Tournament> registerToTournament(@CurrentUser UserPrincipal userPrincipal, @PathVariable Long tournamentId, @RequestBody RegistrationData body) {
        User user = userService.getUserFromPrincipal(userPrincipal);
        Tournament tournament = tournamentService.getTournamentById(tournamentId);
        tournament = tournamentService.registerToTournament(tournament, user, body);
        return ResponseEntity.ok(tournament);
    }
}
