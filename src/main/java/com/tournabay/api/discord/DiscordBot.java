package com.tournabay.api.discord;

import com.tournabay.api.discord.events.GuildEvent;
import com.tournabay.api.repository.GuildJoinRepository;
import com.tournabay.api.service.TournamentService;
import lombok.RequiredArgsConstructor;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.entities.Activity;
import net.dv8tion.jda.api.requests.GatewayIntent;
import net.dv8tion.jda.api.utils.MemberCachePolicy;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
@RequiredArgsConstructor
public class DiscordBot {
    private final GuildJoinRepository guildJoinRepository;
    private final TournamentService tournamentService;

    @Value("${discord.botToken}")
    private String token;

    JDA jda;

    public JDA getJda() {
        if (this.jda == null) {
            this.jda = initBot();
        }
        return this.jda;
    }

    @PostConstruct
    private JDA initBot() {
        this.jda = JDABuilder.createDefault(token)
                .setActivity(Activity.listening("trzasku dupy"))
                .enableIntents(GatewayIntent.GUILD_MEMBERS)
                .setMemberCachePolicy(MemberCachePolicy.ALL)
                .addEventListeners(new GuildEvent(guildJoinRepository, tournamentService))
                .build();
        return this.jda;
    }
}
