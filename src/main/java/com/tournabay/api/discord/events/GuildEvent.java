package com.tournabay.api.discord.events;

import com.tournabay.api.model.Participant;
import com.tournabay.api.model.Tournament;
import com.tournabay.api.model.discord.GuildJoin;
import com.tournabay.api.model.discord.TournamentDiscordData;
import com.tournabay.api.repository.GuildJoinRepository;
import com.tournabay.api.service.TournamentService;
import lombok.RequiredArgsConstructor;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.events.guild.GuildJoinEvent;
import net.dv8tion.jda.api.events.guild.GuildLeaveEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Component
@RequiredArgsConstructor
public class GuildEvent extends ListenerAdapter {
    private final GuildJoinRepository guildJoinRepository;
    private final TournamentService tournamentService;

    @Transactional
    @Override
    public void onGuildJoin(@NotNull GuildJoinEvent event) {
        Optional<GuildJoin> guildJoinOptional = guildJoinRepository.findByGuildId(event.getGuild().getId());
        if (guildJoinOptional.isPresent()) {
            GuildJoin guildJoin = guildJoinOptional.get();
            Tournament tournament = tournamentService.getTournamentById(guildJoin.getTournamentId());
            if (tournament.getDiscordData() != null) return;
            TournamentDiscordData tournamentDiscordData = new TournamentDiscordData(tournament, event.getGuild().getId());
            tournament.setDiscordData(tournamentDiscordData);
            tournamentService.save(tournament);
            if (tournament.getDiscordData().getEnableVerification() && tournament.getDiscordData().getVerifiedRoleId() != null) {
                Role verifiedRole = event.getGuild().getRoleById(tournament.getDiscordData().getVerifiedRoleId());
                if (verifiedRole != null) {
                    for (Participant participant : tournament.getParticipants()) {
                        if (participant.getDiscordId() != null) {
                            Member member = event.getGuild().getMemberById(participant.getDiscordId());
                            if (member != null) {
                                event.getGuild().addRoleToMember(member, verifiedRole).queue();
                            }
                        }
                    }
                }
            }
            guildJoinRepository.delete(guildJoin);
        }
    }

    @Override
    public void onGuildLeave(@NotNull GuildLeaveEvent event) {
        Optional<Tournament> tournamentOptional = tournamentService.getByDiscordDataGuildId(event.getGuild().getId());
        if (tournamentOptional.isPresent()) {
            Tournament tournament = tournamentOptional.get();
            tournament.getDiscordData().setTournament(null);
            tournament.setDiscordData(null);
            tournamentService.save(tournament);
        }
    }

}
