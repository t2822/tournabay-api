package com.tournabay.api.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class DiscordMemberDto {
    private String id;
    private String nickname;
    private String effectiveName;
    private String avatarUrl;
    private Boolean isVerified;
}
