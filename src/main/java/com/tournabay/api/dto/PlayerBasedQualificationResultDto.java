package com.tournabay.api.dto;

import com.tournabay.api.model.Participant;
import lombok.*;
import lombok.experimental.SuperBuilder;

import java.util.List;

@Getter
@Setter
@SuperBuilder
public class PlayerBasedQualificationResultDto extends QualificationResultDto {
    private Participant participant;

    private List<TeamScoresDto> scores;
    private Double qualificationPoints;

}
