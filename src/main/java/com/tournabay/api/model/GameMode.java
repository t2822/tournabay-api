package com.tournabay.api.model;

import lombok.Getter;

@Getter
public enum GameMode {
    OSU("osu"),
    TAIKO("taiko"),
    FRUITS("fruits"),
    MANIA("mania");

    private final String ruleset;

    GameMode(String ruleset) {
        this.ruleset = ruleset;
    }
}
