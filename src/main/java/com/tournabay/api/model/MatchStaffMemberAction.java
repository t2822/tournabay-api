package com.tournabay.api.model;

public enum MatchStaffMemberAction {
    REFEREE_SIGN_IN,
    REFEREE_SIGN_OUT,
    COMMENTATOR_SIGN_IN,
    COMMENTATOR_SIGN_OUT,
    STREAMER_SIGN_IN,
    STREAMER_SIGN_OUT
}
