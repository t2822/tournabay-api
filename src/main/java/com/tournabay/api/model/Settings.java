package com.tournabay.api.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Entity
@Getter
@Setter
@NoArgsConstructor
@SuperBuilder
public class Settings {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JsonIgnore
    @NotNull
    @OneToOne
    private Tournament tournament;

    @NotNull
    private Boolean enableTeamRegistration;

    @NotNull
    private Boolean enableParticipantRegistration;

    @NotNull
    private Boolean allowStaffMembersToRegister;

    @NotNull
    private Boolean requireDiscordAccount;

    @NotNull
    private Boolean openRank;

    private String osuThreadUrl;

    private String discordServerUrl;

    private String bracketUrl;

    private String isBadged;

    @NotNull
    @Min(1)
    private Integer baseTeamSize;

    @NotNull
    @Min(2)
    private Integer maxTeamSize;

    @NotNull
    private Boolean allowStaffMembersToJoin;

    @NotNull
    @Min(1)
    private Integer refereesLimit;

    @NotNull
    @Min(1)
    private Integer commentatorsLimit;

    @NotNull
    @Min(1)
    private Integer streamersLimit;

    @NotNull
    @Min(1)
    private Integer qualificationRoomLimit;

    @NotNull
    @Min(1)
    private Integer qualificationRoomStaffLimit;

    @NotNull
    @Min(1)
    private Integer qualificationRoomSignInLimit;

    public void validate() {
        if (baseTeamSize < 1) {
            throw new IllegalArgumentException("Base team size must be at least 1");
        }
        if (maxTeamSize < 1) {
            throw new IllegalArgumentException("Max team size must be at least 1");
        }
        if (maxTeamSize < baseTeamSize) {
            throw new IllegalArgumentException("Max team size must be greater than or equal to base team size");
        }
    }
}
