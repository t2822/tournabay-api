package com.tournabay.api.model.discord;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.tournabay.api.model.Tournament;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;

@Entity
@Data
@SuperBuilder
@NoArgsConstructor
public class TournamentDiscordData {

    public TournamentDiscordData(Tournament tournament, String guildId) {
        this.tournament = tournament;
        this.guildId = guildId;
        this.enableVerification = false;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String guildId;

    private Boolean enableVerification;
    private String verifiedRoleId;

    private String playerRoleId;
    private String captainRoleId;

    private String announcementChannelId;
    private String staffAnnouncementChannelId;
    private String rescheduleChannelId;
    private String logsChannelId;

    private Boolean notifyReschedulement;
    private Boolean sendLogs;

    @JsonIgnore
    @OneToOne
    private Tournament tournament;
}
