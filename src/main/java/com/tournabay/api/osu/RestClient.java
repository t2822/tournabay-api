package com.tournabay.api.osu;

import com.tournabay.api.osu.model.*;

import java.io.IOException;
import java.util.Optional;

public interface RestClient {
    Optional<OsuBeatmap> getBeatmap(Long beatmapId) throws IOException;
    Optional<BeatmapDifficultyAttributesWrapper> getBeatmapAttributes(Long beatmapId, BeatmapAttributesBody body) throws IOException;
    Optional<MultiplayerLobbyData> getMatchData(Long lobbyId) throws IOException;
    Optional<Topic> getTopic(Long topicId) throws IOException;
    Optional<OsuUserObject> getUser(Long userId, String mode) throws IOException;
}
