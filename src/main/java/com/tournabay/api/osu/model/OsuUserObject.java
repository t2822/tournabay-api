package com.tournabay.api.osu.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.LinkedHashMap;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class OsuUserObject {
    private Long id;
    private String avatar_url;
    private String country_code;
    private Boolean is_bot;
    private Boolean pm_friends_only;
    private String username;
    private String cover_url;
    private LinkedHashMap<String, Object> statistics;

}
