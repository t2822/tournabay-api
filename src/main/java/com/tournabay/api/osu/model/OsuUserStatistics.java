package com.tournabay.api.osu.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class OsuUserStatistics {

    @Getter
    @NoArgsConstructor
    @AllArgsConstructor
    public class Level {
        private Integer current;
        private Double progress;
    }

    private Level level;
    private Double pp;
    private Integer global_rank;
    private Double hit_accuracy;


}
