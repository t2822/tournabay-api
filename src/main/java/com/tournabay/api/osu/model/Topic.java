package com.tournabay.api.osu.model;

import lombok.Getter;

import java.util.List;

@Getter
public class Topic {
    private List<PostDetails> posts;

    @Getter
    public class PostDetails {
        private PostDetailsBody body;
    }

    @Getter
    public class PostDetailsBody {
        private String html;
    }
}
