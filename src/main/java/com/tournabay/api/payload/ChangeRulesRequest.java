package com.tournabay.api.payload;

import lombok.Getter;

@Getter
public class ChangeRulesRequest {
    private String rules;
}
