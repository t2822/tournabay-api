package com.tournabay.api.payload;

import lombok.Getter;

@Getter
public class DiscordBasicRoles {
    private String playerRoleId;
    private String captainRoleId;
}
