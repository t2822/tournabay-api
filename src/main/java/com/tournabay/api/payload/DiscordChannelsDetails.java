package com.tournabay.api.payload;

import lombok.Getter;

@Getter
public class DiscordChannelsDetails {
    private String announcementChannelId;
    private String staffAnnouncementChannelId;
    private String rescheduleChannelId;
    private String logsChannelId;
}
