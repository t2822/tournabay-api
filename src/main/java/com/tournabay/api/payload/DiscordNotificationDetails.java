package com.tournabay.api.payload;

import lombok.Getter;

@Getter
public class DiscordNotificationDetails {
    private Boolean notifyReschedulement;
    private Boolean sendLogs;
}
