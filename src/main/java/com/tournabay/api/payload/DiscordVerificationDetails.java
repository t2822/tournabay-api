package com.tournabay.api.payload;

import lombok.Getter;

@Getter
public class DiscordVerificationDetails {
    private boolean enableVerification;
    private String verifiedRoleId;
}
