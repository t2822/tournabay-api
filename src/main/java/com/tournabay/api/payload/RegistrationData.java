package com.tournabay.api.payload;

import lombok.Getter;

import javax.validation.constraints.NotNull;
import java.util.List;

@Getter
public class RegistrationData {
    List<Integer> timezoneRange;
    @NotNull(message = "Please provide your discord tag!")
    private String discordTag;
}
