package com.tournabay.api.payload;

import lombok.Getter;

@Getter
public class UpdateParticipantRequest {
    private String discordTag;
    private Long teamId;
}
