package com.tournabay.api.repository;

import com.tournabay.api.model.discord.GuildJoin;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface GuildJoinRepository extends JpaRepository<GuildJoin, Long> {
    Optional<GuildJoin> findByGuildId(String guildId);
}
