package com.tournabay.api.repository;

import com.tournabay.api.model.qualifications.QualificationRoom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface QualificationRoomRepository extends JpaRepository<QualificationRoom, Long> {
    List<QualificationRoom> findAllByTournamentId(Long tournamentId);
}
