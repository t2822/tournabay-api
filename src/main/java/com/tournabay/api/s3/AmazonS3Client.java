package com.tournabay.api.s3;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class AmazonS3Client {
    private AmazonS3 s3Client;

    @Value("${s3.accessKey}")
    private String accessKey;

    @Value("${s3.secretKey}")
    private String secretKey;

    @PostConstruct
    public AmazonS3 getS3Client() {
        if (this.s3Client == null) {
            this.s3Client = AmazonS3ClientBuilder
                    .standard()
                    .withCredentials(new AWSStaticCredentialsProvider(getCredentials()))
                    .withRegion(Regions.US_EAST_1)
                    .build();
        }
        return this.s3Client;
    }

    private AWSCredentials getCredentials() {
        return new BasicAWSCredentials(
                accessKey,
                secretKey
        );
    }
}
