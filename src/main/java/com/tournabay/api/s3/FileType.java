package com.tournabay.api.s3;

public enum FileType {
    LOGO("logo"),
    BANNER("banner");

    private final String type;

    public String getType() {
        return type;
    }

    FileType(String type) {
        this.type = type;
    }
}