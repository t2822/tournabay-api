package com.tournabay.api.service;

import com.tournabay.api.model.Team;
import com.tournabay.api.model.Tournament;
import com.tournabay.api.s3.FileType;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;

@Service
public interface AmazonS3Service {

    File getFileFromMultipartFile(MultipartFile multipartFile);

    File uploadFile(Tournament tournament, File file, FileType fileType);

    void deleteTemporaryFile(File file);

    void downloadFile(String keyName, String downloadFilePath);

    File uploadFile(Tournament tournament, Team team, File file, FileType fileType);
}
