package com.tournabay.api.service;

import com.tournabay.api.dto.DiscordMemberDto;
import com.tournabay.api.model.Tournament;
import com.tournabay.api.model.discord.TournamentDiscordData;
import com.tournabay.api.payload.DiscordBasicRoles;
import com.tournabay.api.payload.DiscordChannelsDetails;
import com.tournabay.api.payload.DiscordNotificationDetails;
import com.tournabay.api.payload.DiscordVerificationDetails;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface DiscordService {

    List<DiscordMemberDto> getServerMembers(Tournament tournament);
    TournamentDiscordData getTournamentDiscordData(Tournament tournament);
    TournamentDiscordData updateVerificationDetails(Tournament tournament, DiscordVerificationDetails details);
    TournamentDiscordData updateBasicRoles(Tournament tournament, DiscordBasicRoles discordBasicRoles);
    TournamentDiscordData updateChannels(Tournament tournament, DiscordChannelsDetails discordChannelsDetails);
    TournamentDiscordData updateNotificationDetails(Tournament tournament, DiscordNotificationDetails discordNotificationDetails);

}
