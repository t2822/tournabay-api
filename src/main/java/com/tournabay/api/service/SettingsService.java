package com.tournabay.api.service;

import com.tournabay.api.model.Settings;
import com.tournabay.api.model.Tournament;
import com.tournabay.api.repository.SettingsRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class SettingsService {
    private final SettingsRepository settingsRepository;

    /**
     * Create a new settings object. It's used when a new tournament is created.
     *
     * @return Settings
     */
    @SuppressWarnings("UnusedReturnValue")
    public Settings createDefaultSettings() {
        return Settings.builder()
                .enableTeamRegistration(false)
                .enableParticipantRegistration(true)
                .allowStaffMembersToRegister(false)
                .requireDiscordAccount(false)
                .openRank(true)
                .baseTeamSize(4)
                .maxTeamSize(8)
                .allowStaffMembersToJoin(false)
                .refereesLimit(1)
                .commentatorsLimit(2)
                .streamersLimit(1)
                .qualificationRoomLimit(4)
                .qualificationRoomStaffLimit(1)
                .qualificationRoomSignInLimit(8)
                .build();
    }

    /**
     * Update the settings for a tournament.
     *
     * @param tournament The tournament that the settings are for.
     * @param settings   The settings object that you want to update.
     * @return Settings
     */
    public Settings updateSettings(Tournament tournament, Settings settings) {
        settings.validate();
        if (settings.getEnableParticipantRegistration()) {
            settings.setEnableTeamRegistration(false);
        }
        settings.setTournament(tournament);
        return settingsRepository.save(settings);
    }

}
