package com.tournabay.api.service;

import com.tournabay.api.exception.BadRequestException;
import com.tournabay.api.exception.ResourceNotFoundException;
import com.tournabay.api.model.*;
import com.tournabay.api.osu.OsuApiClient;
import com.tournabay.api.payload.CreateTournamentRequest;
import com.tournabay.api.payload.RegistrationData;
import com.tournabay.api.repository.TournamentRepository;
import com.tournabay.api.service.implementation.PermissionServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class TournamentService {
    private final TournamentRepository tournamentRepository;
    private final TournamentRoleService tournamentRoleService;
    private final StaffMemberService staffMemberService;
    private final PermissionServiceImpl permissionServiceImpl;
    private final SettingsService settingsService;
    private final ParticipantService participantService;

    /**
     * Get all tournaments from the database and return them.
     *
     * @return A list of all tournaments
     */
    public List<Tournament> getAllTournaments() {
        return tournamentRepository.findAll();
    }

    /**
     * Get the tournament with the given id, or throw an exception if it doesn't exist.
     *
     * @param id The id of the tournament you want to get.
     * @return A tournament object with the roles sorted by position.
     */
    public Tournament getTournamentById(Long id) {
        return tournamentRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Tournament not found!"));
    }

    /**
     * Save the tournament to the database.
     *
     * @param tournament The tournament object that is being saved.
     * @return The tournament object is being returned.
     */
    public Tournament save(Tournament tournament) {
        return tournamentRepository.save(tournament);
    }

    /**
     * Find a tournament by its discord data guild id.
     *
     * @param guildId The id of the discord server
     * @return An Optional<Tournament> object.
     */
    public Optional<Tournament> getByDiscordDataGuildId(String guildId) {
        return tournamentRepository.findByDiscordDataGuildId(guildId);
    }

    // TODO: Code clean-up

    /**
     * It creates a tournament from request body.
     *
     * @param body  The request body, which is a CreateTournamentRequest object.
     * @param owner The user who created the tournament
     * @return A tournament object
     */
    @Transactional
    public Tournament createTournament(CreateTournamentRequest body, User owner) {
        if (body.getTeamFormat().equals(TeamFormat.TEAM_VS)) {
            List<TournamentRole> tournamentRoles = tournamentRoleService.createDefaultTournamentRoles();
            TournamentRole defaultRole = tournamentRoles.stream()
                    .filter(role -> role.getName().equalsIgnoreCase("Uncategorized"))
                    .findFirst()
                    .orElseThrow(() -> new RuntimeException("Default role not found"));
            TournamentRole masterRole = tournamentRoles.stream()
                    .filter(role -> role.getName().equalsIgnoreCase("Host"))
                    .findFirst()
                    .orElseThrow(() -> new RuntimeException("Master role not found"));
            List<Permission> defaultPermissions = permissionServiceImpl.createDefaultPermissions(tournamentRoles);
            StaffMember host = staffMemberService.createHost(owner.getOsuId(), owner, body.getGameMode(), masterRole);
            Settings settings = settingsService.createDefaultSettings();
            TeamBasedTournament tournament = TeamBasedTournament
                    .builder()
                    .name(body.getName())
                    .initials(body.getInitials())
                    .gameMode(body.getGameMode())
                    .scoreType(body.getScoreType())
                    .teamFormat(body.getTeamFormat())
                    .maxStage(body.getMaxStage())
                    .registrationStartDate(body.getRegistrationStartDate())
                    .registrationEndDate(body.getRegistrationEndDate())
                    .startDate(body.getStartDate())
                    .endDate(body.getEndDate())
                    .staffMembers(new ArrayList<>(List.of(host)))
                    .roles(tournamentRoles)
                    .defaultRole(defaultRole)
                    .masterRole(masterRole)
                    .owner(owner)
                    .permissions(defaultPermissions)
                    .settings(settings)
                    .build();
            tournamentRoles.forEach(role -> role.setTournament(tournament));
            defaultPermissions.forEach(permission -> permission.setTournament(tournament));
            host.setTournament(tournament);
            settings.setTournament(tournament);
            return tournamentRepository.save(tournament);
        } else if (body.getTeamFormat().equals(TeamFormat.PLAYER_VS)) {
            List<TournamentRole> tournamentRoles = tournamentRoleService.createDefaultTournamentRoles();
            TournamentRole defaultRole = tournamentRoles.stream()
                    .filter(role -> role.getName().equalsIgnoreCase("Uncategorized"))
                    .findFirst()
                    .orElseThrow(() -> new RuntimeException("Default role not found"));
            TournamentRole masterRole = tournamentRoles.stream()
                    .filter(role -> role.getName().equalsIgnoreCase("Host"))
                    .findFirst()
                    .orElseThrow(() -> new RuntimeException("Master role not found"));
            List<Permission> defaultPermissions = permissionServiceImpl.createDefaultPermissions(tournamentRoles);
            StaffMember host = staffMemberService.createHost(owner.getOsuId(), owner, body.getGameMode(), masterRole);
            Settings settings = settingsService.createDefaultSettings();
            PlayerBasedTournament tournament = PlayerBasedTournament
                    .builder()
                    .name(body.getName())
                    .initials(body.getInitials())
                    .gameMode(body.getGameMode())
                    .scoreType(body.getScoreType())
                    .teamFormat(body.getTeamFormat())
                    .maxStage(body.getMaxStage())
                    .registrationStartDate(body.getRegistrationStartDate())
                    .registrationEndDate(body.getRegistrationEndDate())
                    .startDate(body.getStartDate())
                    .endDate(body.getEndDate())
                    .staffMembers(new ArrayList<>(List.of(host)))
                    .roles(tournamentRoles)
                    .defaultRole(defaultRole)
                    .masterRole(masterRole)
                    .owner(owner)
                    .permissions(defaultPermissions)
                    .settings(settings)
                    .build();
            tournamentRoles.forEach(role -> role.setTournament(tournament));
            defaultPermissions.forEach(permission -> permission.setTournament(tournament));
            host.setTournament(tournament);
            settings.setTournament(tournament);
            return tournamentRepository.save(tournament);
        }

        throw new BadRequestException("Unsupported team format");
    }

    public Tournament registerToTournament(Tournament tournament, User user, RegistrationData registrationData) {
        LocalDateTime now = LocalDateTime.now();
        if (now.isBefore(tournament.getRegistrationStartDate())) {
            throw new BadRequestException("Registration has not started yet");
        } else if (now.isAfter(tournament.getRegistrationEndDate())) {
            throw new BadRequestException("Registration has ended");
        }
//        else if (tournament.getSettings().getRequireDiscordAccount() && user.getDiscordData().isEmpty()) {
//            throw new BadRequestException("You need to link your discord account to register to this tournament");
//        }
        boolean isStaffMember = tournament.getStaffMembers().stream().anyMatch(staff -> staff.getUser().equals(user));
        if (!tournament.getSettings().getAllowStaffMembersToRegister() && isStaffMember) {
            throw new BadRequestException("You are a staff member of this tournament");
        }
        if (tournament instanceof TeamBasedTournament) {
            // check if it's auction
            if (!tournament.getSettings().getEnableParticipantRegistration())
                throw new BadRequestException("Participant registration is disabled");
            // check if user is already registered
            if (tournament.getParticipants().stream().anyMatch(participant -> participant.getUser().equals(user)))
                throw new BadRequestException("You are already registered to this tournament");
            // check if user is already in a team
            if (tournament.getParticipants().stream().anyMatch(participant -> participant.getTeam() != null &&
                    participant.getTeam().getParticipants().stream().anyMatch(member -> member.getUser().equals(user))))
                throw new BadRequestException("You are already in a team");
            // TODO: check rank limit from tournament settings
            Participant participant = participantService.createParticipantFromOsuId(user.getOsuId(), tournament, user);
            participant.setMinTimezone(registrationData.getTimezoneRange().get(0));
            participant.setMaxTimezone(registrationData.getTimezoneRange().get(1));
            if (tournament.getSettings().getRequireDiscordAccount()) {
                participant.setDiscordTag(registrationData.getDiscordTag());
            }
            participant = participantService.save(participant);
            tournament.getParticipants().add(participant);
            return tournament;

        }
        throw new UnsupportedOperationException("Action not supported");
    }

    public List<Tournament> getTournamentsByUser(User user) {
        List<Tournament> tournaments = this.getAllTournaments();
        return tournaments.stream()
                .filter(tournament -> tournament.getOwner().equals(user) ||
                        tournament.getStaffMembers().stream().anyMatch(staff -> staff.getUser().equals(user)) ||
                        tournament.getParticipants().stream().anyMatch(participant -> participant.getUser().equals(user)))
                .collect(Collectors.toList());
    }

    public String getBBCode(Tournament tournament, User user) {
        try (OsuApiClient osuApiClient = new OsuApiClient(user.getOsuToken())) {
            String topicId = tournament.getSettings().getOsuThreadUrl().split("/")[6];
            return osuApiClient.getTopic(Long.parseLong(topicId))
                    .map(topic -> topic.getPosts().get(0).getBody().getHtml())
                    .orElseThrow(() -> new RuntimeException("Couldn't get score from Osu! API"));
        } catch (IOException e) {
            throw new RuntimeException("Something went wrong");
        }
    }
}
