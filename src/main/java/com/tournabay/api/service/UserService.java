package com.tournabay.api.service;

import com.tournabay.api.exception.OsuUserNotFound;
import com.tournabay.api.exception.ResourceNotFoundException;
import com.tournabay.api.jackson.user.UserReader;
import com.tournabay.api.model.*;
import com.tournabay.api.osu.OsuApi;
import com.tournabay.api.osu.OsuApiClient;
import com.tournabay.api.osu.model.OsuUserObject;
import com.tournabay.api.repository.UserRepository;
import com.tournabay.api.security.UserPrincipal;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class UserService {
    private final UserRepository userRepository;
    private final RoleService roleService;
    private final UserStatisticsService userStatisticsService;

    /**
     * Get the user from the database using the user principal's id.
     *
     * @param userPrincipal The UserPrincipal object that is passed to the method.
     * @return A user object
     */
    public User getUserFromPrincipal(UserPrincipal userPrincipal) {
        return userRepository.findById(userPrincipal.getId()).orElseThrow(() -> new ResourceNotFoundException("User not found!"));
    }

    /**
     * If the user is not found, throw a ResourceNotFoundException. Otherwise, return the user.
     *
     * @param id The id of the user to be retrieved.
     * @return A user object
     */
    public User getById(Long id) {
        return userRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("User not found"));
    }

    /**
     * If the user is not found, throw an exception.
     *
     * @param osuId The osu! user id
     * @return A user object
     */
    public User getByOsuId(Long osuId) {
        return userRepository.findByOsuId(osuId).orElseThrow(() -> new OsuUserNotFound("User not found"));
    }

    /**
     * It takes an osu! user ID, checks if the user exists in the database, if not, it creates a new user with the data
     * from the osu! API
     *
     * @param osuId The user's osu! ID.
     * @return User object
     */
    public User addUserByOsuId(Long osuId, User user, Tournament tournament) {
        Optional<User> userOptional = userRepository.findByOsuId(osuId);
        if (userOptional.isPresent()) {
            return userOptional.get();
        }
        try (OsuApiClient osuApiClient = new OsuApiClient(user.getOsuToken())) {
            OsuUserObject osuUser = osuApiClient.getUser(osuId, tournament.getGameMode().getRuleset())
                    .orElseThrow(() -> new OsuUserNotFound("User not found"));
            UserStatistics userStatistics = userStatisticsService.createStatistics(osuUser.getStatistics(), tournament.getGameMode());
            Set<Role> roles = roleService.getBasicRoles();
            User newUser = new User(
                    osuUser.getUsername(),
                    osuUser.getId(),
                    osuUser.getAvatar_url(),
                    AuthProvider.osu,
                    roles,
                    osuUser.getCountry_code(),
                    userStatistics
            );
            userStatistics.setUser(newUser);
            return userRepository.save(newUser);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public User addUserByOsuId(Long osuId, User user, GameMode gameMode) {
        Optional<User> userOptional = userRepository.findByOsuId(osuId);
        if (userOptional.isPresent()) {
            return userOptional.get();
        }
        try (OsuApiClient osuApiClient = new OsuApiClient(user.getOsuToken())) {
            OsuUserObject osuUser = osuApiClient.getUser(osuId, gameMode.getRuleset())
                    .orElseThrow(() -> new OsuUserNotFound("User not found"));
            UserStatistics userStatistics = userStatisticsService.createStatistics(osuUser.getStatistics(), gameMode);
            Set<Role> roles = roleService.getBasicRoles();
            User newUser = new User(
                    osuUser.getUsername(),
                    osuUser.getId(),
                    osuUser.getAvatar_url(),
                    AuthProvider.osu,
                    roles,
                    osuUser.getCountry_code(),
                    userStatistics
            );
            userStatistics.setUser(newUser);
            return userRepository.save(newUser);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
