package com.tournabay.api.service;

import com.tournabay.api.model.GameMode;
import com.tournabay.api.model.UserStatistics;
import com.tournabay.api.osu.model.OsuUserStatistics;
import org.springframework.stereotype.Service;

import java.util.LinkedHashMap;
import java.util.List;

@Service
public interface UserStatisticsService {
    List<UserStatistics> createStatistics(LinkedHashMap<String, LinkedHashMap<String, Object>> statistics);
    UserStatistics createOsuStatistics(LinkedHashMap<String, Object> statistics);
    UserStatistics createTaikoStatistics(LinkedHashMap<String, Object> statistics);
    UserStatistics createFruitsStatistics(LinkedHashMap<String, Object> statistics);
    UserStatistics createManiaStatistics(LinkedHashMap<String, Object> statistics);

    UserStatistics createStatistics(LinkedHashMap<String, Object> statistics, GameMode gameMode);

    List<UserStatistics> updateUserStatistics(List<UserStatistics> userStatistics, LinkedHashMap<String, LinkedHashMap<String, Object>> statistics);
}
