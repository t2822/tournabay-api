package com.tournabay.api.service.implementation;

import com.amazonaws.SdkClientException;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.tournabay.api.model.Team;
import com.tournabay.api.model.Tournament;
import com.tournabay.api.s3.AmazonS3Client;
import com.tournabay.api.s3.FileType;
import com.tournabay.api.service.AmazonS3Service;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class AmazonS3ServiceImpl implements AmazonS3Service {
    private final AmazonS3Client s3Client;

    private final String BUCKET_NAME = "tournabay";

    @Override
    public File getFileFromMultipartFile(MultipartFile multipartFile) {
        try {
            File file = new File(System.getProperty("java.io.tmpdir") + "/" + UUID.randomUUID() + multipartFile.getOriginalFilename());
            multipartFile.transferTo(file);
            return file;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public File uploadFile(Tournament tournament, File file, FileType fileType) {
        try {
            s3Client.getS3Client().putObject(
                    new PutObjectRequest(
                            BUCKET_NAME,
                            "tournament/" + tournament.getId() + "/" + fileType.getType() + ".jpg",
                            file)
                            .withCannedAcl(CannedAccessControlList.PublicRead)
            );
            return file;
        } catch (SdkClientException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void deleteTemporaryFile(File file) {
        file.deleteOnExit();
    }

    @Override
    public void downloadFile(String keyName, String downloadFilePath) {

    }

    @Override
    public File uploadFile(Tournament tournament, Team team, File file, FileType fileType) {
        try {
            s3Client.getS3Client().putObject(
                    new PutObjectRequest(
                            BUCKET_NAME,
                            "tournament/" + tournament.getId() + "/team/" + team.getId() + "/" + fileType.getType() + ".jpg",
                            file)
                            .withCannedAcl(CannedAccessControlList.PublicRead)
            );
            return file;
        } catch (SdkClientException e) {
            throw new RuntimeException(e);
        }
    }
}
