package com.tournabay.api.service.implementation;

import com.tournabay.api.discord.DiscordBot;
import com.tournabay.api.dto.DiscordMemberDto;
import com.tournabay.api.exception.BadRequestException;
import com.tournabay.api.model.Tournament;
import com.tournabay.api.model.discord.TournamentDiscordData;
import com.tournabay.api.payload.DiscordBasicRoles;
import com.tournabay.api.payload.DiscordChannelsDetails;
import com.tournabay.api.payload.DiscordNotificationDetails;
import com.tournabay.api.payload.DiscordVerificationDetails;
import com.tournabay.api.repository.TournamentDiscordDataRepository;
import com.tournabay.api.service.DiscordService;
import lombok.RequiredArgsConstructor;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class DiscordServiceImpl implements DiscordService {
    private final DiscordBot discordBot;
    private final TournamentDiscordDataRepository tournamentDiscordDataRepository;

    @Override
    public List<DiscordMemberDto> getServerMembers(Tournament tournament) {
        if (tournament.getDiscordData() == null) {
            throw new BadRequestException("Tournament doesn't have a discord server!");
        }
        TournamentDiscordData discordData = tournament.getDiscordData();
        Guild tournamentGuild = discordBot.getJda().getGuildById(discordData.getGuildId());
        if (tournamentGuild == null) throw new BadRequestException("Guild is null!");
        List<Member> members = tournamentGuild.loadMembers().get();
        return members.stream()
                .map(member -> {
                    boolean isVerified = member.getRoles().stream().anyMatch(role -> role.getId().equals(discordData.getVerifiedRoleId()));
                    return new DiscordMemberDto(member.getId(), member.getNickname(), member.getEffectiveName(), member.getAvatarUrl(), isVerified);
                })
                .collect(Collectors.toList());
    }

    @Override
    public TournamentDiscordData getTournamentDiscordData(Tournament tournament) {
        return tournament.getDiscordData();
    }

    @Override
    public TournamentDiscordData updateVerificationDetails(Tournament tournament, DiscordVerificationDetails details) {
        TournamentDiscordData discordData = tournament.getDiscordData();
        if (discordData == null) {
            throw new BadRequestException("Tournament doesn't have a discord server!");
        }
        discordData.setEnableVerification(details.isEnableVerification());
        discordData.setVerifiedRoleId(details.getVerifiedRoleId());
        return tournamentDiscordDataRepository.save(discordData);
    }

    @Override
    public TournamentDiscordData updateBasicRoles(Tournament tournament, DiscordBasicRoles discordBasicRoles) {
        TournamentDiscordData discordData = tournament.getDiscordData();
        if (discordData == null) {
            throw new BadRequestException("Tournament doesn't have a discord server!");
        }
        discordData.setPlayerRoleId(discordBasicRoles.getPlayerRoleId());
        discordData.setCaptainRoleId(discordBasicRoles.getCaptainRoleId());
        return tournamentDiscordDataRepository.save(discordData);
    }

    @Override
    public TournamentDiscordData updateChannels(Tournament tournament, DiscordChannelsDetails discordChannelsDetails) {
        TournamentDiscordData discordData = tournament.getDiscordData();
        if (discordData == null) {
            throw new BadRequestException("Tournament doesn't have a discord server!");
        }
        discordData.setAnnouncementChannelId(discordChannelsDetails.getAnnouncementChannelId());
        discordData.setStaffAnnouncementChannelId(discordChannelsDetails.getStaffAnnouncementChannelId());
        discordData.setRescheduleChannelId(discordChannelsDetails.getRescheduleChannelId());
        discordData.setLogsChannelId(discordChannelsDetails.getLogsChannelId());
        return tournamentDiscordDataRepository.save(discordData);
    }

    @Override
    public TournamentDiscordData updateNotificationDetails(Tournament tournament, DiscordNotificationDetails discordNotificationDetails) {
        TournamentDiscordData discordData = tournament.getDiscordData();
        if (discordData == null) {
            throw new BadRequestException("Tournament doesn't have a discord server!");
        }
        discordData.setNotifyReschedulement(discordNotificationDetails.getNotifyReschedulement());
        discordData.setSendLogs(discordNotificationDetails.getSendLogs());
        return tournamentDiscordDataRepository.save(discordData);
    }

}
