package com.tournabay.api.service.implementation;

import com.google.gson.internal.LinkedTreeMap;
import com.tournabay.api.model.GameMode;
import com.tournabay.api.model.UserStatistics;
import com.tournabay.api.osu.model.OsuUserStatistics;
import com.tournabay.api.service.UserStatisticsService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UserStatisticsServiceImpl implements UserStatisticsService {

    @Override
    public List<UserStatistics> createStatistics(LinkedHashMap<String, LinkedHashMap<String, Object>> statistics) {
        List<UserStatistics> userStatistics = new ArrayList<>();
        LinkedHashMap<String, Object> osuRulesetStatistics = statistics.get("osu");
        LinkedHashMap<String, Object> taikoRulesetStatistics = statistics.get("taiko");
        LinkedHashMap<String, Object> fruitsRulesetStatistics = statistics.get("fruits");
        LinkedHashMap<String, Object> maniaRulesetStatistics = statistics.get("mania");
        userStatistics.add(createOsuStatistics(osuRulesetStatistics));
        userStatistics.add(createTaikoStatistics(taikoRulesetStatistics));
        userStatistics.add(createFruitsStatistics(fruitsRulesetStatistics));
        userStatistics.add(createManiaStatistics(maniaRulesetStatistics));
        return userStatistics;
    }

    @Override
    public UserStatistics createOsuStatistics(LinkedHashMap<String, Object> statistics) {
        return UserStatistics.builder()
                .ruleset(GameMode.OSU)
                .globalRank(getGlobalRank(statistics.get("global_rank")))
                .pp(Double.parseDouble(statistics.get("pp").toString()))
                .accuracy((Double) statistics.get("hit_accuracy"))
                .build();
    }

    @Override
    public UserStatistics createTaikoStatistics(LinkedHashMap<String, Object> statistics) {
             return UserStatistics.builder()
                .ruleset(GameMode.TAIKO)
                .globalRank(getGlobalRank(statistics.get("global_rank")))
                .pp(Double.parseDouble(statistics.get("pp").toString()))
                .accuracy((Double) statistics.get("hit_accuracy"))
                .build();
    }

    @Override
    public UserStatistics createFruitsStatistics(LinkedHashMap<String, Object> statistics) {
        return UserStatistics.builder()
                .ruleset(GameMode.FRUITS)
                .globalRank(getGlobalRank(statistics.get("global_rank")))
                .pp(Double.parseDouble(statistics.get("pp").toString()))
                .accuracy((Double) statistics.get("hit_accuracy"))
                .build();
    }

    @Override
    public UserStatistics createManiaStatistics(LinkedHashMap<String, Object> statistics) {
        return UserStatistics.builder()
                .ruleset(GameMode.MANIA)
                .globalRank(getGlobalRank(statistics.get("global_rank")))
                .pp(Double.parseDouble(statistics.get("pp").toString()))
                .accuracy((Double) statistics.get("hit_accuracy"))
                .build();
    }

    @Override
    public UserStatistics createStatistics(LinkedHashMap<String, Object> statistics, GameMode gameMode) {
        return UserStatistics.builder()
                .ruleset(gameMode)
                .globalRank(getGlobalRank(statistics.get("global_rank")))
                .pp(Double.parseDouble(statistics.get("pp").toString()))
                .accuracy((Double) statistics.get("hit_accuracy"))
                .build();
    }

    @Override
    public List<UserStatistics> updateUserStatistics(List<UserStatistics> userStatistics, LinkedHashMap<String, LinkedHashMap<String, Object>> statistics) {
        return userStatistics.stream()
                .peek(stats -> {
                    GameMode ruleset = stats.getRuleset();
                    LinkedHashMap<String, Object> statisticsRuleset = statistics.get(ruleset.getRuleset());
                    stats.setGlobalRank(getGlobalRank(statistics.get("global_rank")));
                    stats.setPp(Double.parseDouble(statistics.get("pp").toString()));
                    stats.setAccuracy((Double) statisticsRuleset.get("hit_accuracy"));
                })
                .collect(Collectors.toList());
    }

    private Double getGlobalRank(Object statObj) {
        if (statObj == null) return null;
        return Double.parseDouble(statObj.toString());
    }

}
